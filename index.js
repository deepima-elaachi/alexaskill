const alexa = require('ask-sdk');
const { registerRequest } = require('./helperFunctions/APIcalls/register');
const { timestamp } = require('./helperFunctions/timestamp');
//const { getRoom } = require('./helperFunctions/getRoom');
const { searchFoodItem } = require('./helperFunctions/searchFoodItem');
const { registerFeedback } = require('./helperFunctions/APIcalls/registerFeedback');
const { getResponse } = require('./helperFunctions/APIcalls/getResponse');
const { getFoodMenu } = require('./helperFunctions/APIcalls/getFoodMenu');
const searchData = require('./helperFunctions/searchData');
const { randomArray } = require('./helperFunctions/randomArray');
const { checkReqStatus } = require('./helperFunctions/checkStatus/checkReqStatus');
const { genericRequestStatus } = require('./helperFunctions/checkStatus/genericReqStatus');
const { getRoomInfo } = require('./helperFunctions/APIcalls/getRoomInfo');

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    async handle(handlerInput) {
        let message;
        let reprompt;

        message = `Welcome to hotel, what do you want?`;
        reprompt = 'What? ';

        return handlerInput.responseBuilder
            .speak(message)
            .reprompt(reprompt)
            .getResponse();
    },
};

const ProactiveEventHandler = {
    canHandle(handlerInput) {
        console.log(handlerInput)
        return handlerInput.requestEnvelope.request.type === 'AlexaSkillEvent.ProactiveSubscriptionChanged'
    }, handle(handlerInput) {
        console.log("AWS User" + handlerInput.requestEnvelope.context.System.user.userId)
        console.log("API EndPoint" + handlerInput.requestEnvelope.context.System.apiEndpoint)
        console.log("AWS User" + JSON.stringify(handlerInput.requestEnvelope.request.body.subscriptions))
    }
};

const InProgressAmenitiesIntent = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && (request.intent.name === 'AmenitiesIntent' || request.intent.name === 'BookSpaIntent'
                || request.intent.name === 'BookCabIntent' || request.intent.name === 'WakeupCallIntent'
                || request.intent.name === 'ExtendedCheckOutIntent' || request.intent.name === 'RemoveFoodItem')
            && request.dialogState !== 'COMPLETED';
    },
    handle(handlerInput) {
        const currentIntent = handlerInput.requestEnvelope.request.intent;
        let prompt = '';

        for (const slotName in currentIntent.slots) {
            if (Object.prototype.hasOwnProperty.call(currentIntent.slots, slotName)) {
                const currentSlot = currentIntent.slots[slotName];
                if (currentSlot.confirmationStatus !== 'CONFIRMED'
                    && currentSlot.resolutions
                    && currentSlot.resolutions.resolutionsPerAuthority[0]) {
                    if (currentSlot.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH') {
                        if (currentSlot.resolutions.resolutionsPerAuthority[0].values.length > 1) {
                            prompt = 'Which would you like';
                            const size = currentSlot.resolutions.resolutionsPerAuthority[0].values.length;

                            currentSlot.resolutions.resolutionsPerAuthority[0].values
                                .forEach((element, index) => {
                                    prompt += ` ${(index === size - 1) ? ' or' : ' '} ${element.value.name}`;
                                });

                            prompt += '?';

                            return handlerInput.responseBuilder
                                .speak(prompt)
                                .reprompt(prompt)
                                .addElicitSlotDirective(currentSlot.name)
                                .getResponse();
                        }
                    } else if (currentSlot.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_NO_MATCH') {
                        if (requiredSlots.indexOf(currentSlot.name) > -1) {
                            prompt = `What ${currentSlot.name} are you looking for`;

                            return handlerInput.responseBuilder
                                .speak(prompt)
                                .reprompt(prompt)
                                .addElicitSlotDirective(currentSlot.name)
                                .getResponse();
                        }
                    }
                }
            }
        }

        return handlerInput.responseBuilder
            .addDelegateDirective(currentIntent)
            .getResponse();
    },
};

const AmenitiesIntent = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'AmenitiesIntent'
            && request.dialogState === 'COMPLETED'
    }, async handle(handlerInput) {
        let deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
        console.log('deviceId is', deviceId);
        let request = handlerInput.requestEnvelope.request;
        let reqid_param = 'AM';
        let req_id_str = await timestamp(request.timestamp);
        let requestDepartment = 'Housekeeping'
        let intentName = 'AmenitiesIntent';
        console.log(req_id_str);
        if (request.intent.slots.Toiletries
            && request.intent.slots.Toiletries.resolutions
            && request.intent.slots.Toiletries.resolutions.resolutionsPerAuthority[0]
            && request.intent.slots.Toiletries.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH'
            && request.intent.slots.Toiletries.value !== "?" && request.intent.slots.Toiletries.value) {
            let item = request.intent.slots.Toiletries.resolutions.resolutionsPerAuthority[0].values[0].value.name;
            let amount = request.intent.slots.Number.value;
            let requestTypeValue = 'Amenities'
            let slotValues = {
                item: item,
                amount: amount,
            }
            const message = await registerRequest(requestTypeValue, requestDepartment, slotValues, deviceId, intentName);
            console.log('RESPONSE====> ', message.speechText);
            let speechText = await randomArray(message.speechText);
            console.log(speechText);
            speechText = speechText.replace('@amenity', item);
            speechText = speechText.replace('@number', amount);
            console.log('finalmessage', speechText);

            if (message) {
                return handlerInput.responseBuilder
                    .speak(`${speechText} Would you like to have anything else? `)
                    .reprompt('Anything else I can help you with? ')
                    .getResponse();
            } else {
                return handlerInput.responseBuilder
                    .speak('order not placed')
                    .reprompt('Anything else I can help you with? ')
                    .getResponse();
            }
        }
        return handlerInput.responseBuilder
            .speak(`Sorry, I couldn't get you. Please say again? `)
            .reprompt(`Sorry, I couldn't get you. Please say again? `)
            .getResponse();
    }
};

const GeneralHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'HousekeepingIntent'
            && handlerInput.requestEnvelope.request.intent.name === 'EngineeringIntent'
            && handlerInput.requestEnvelope.request.intent.name === 'LaundryIntent'

    }, async handle(handlerInput) {
        let request = handlerInput.requestEnvelope.request;
        let intent = request.intent.name;
        let deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;

        switch (intent) {
            case 'ServiceIntent': {
                if (request.intent.slots.Housekeeping
                    && request.intent.slots.Housekeeping.resolutions
                    && request.intent.slots.Housekeeping.resolutions.resolutionsPerAuthority[0]
                    && request.intent.slots.Housekeeping.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH'
                    && request.intent.slots.Housekeeping.value !== "?" && request.intent.slots.Housekeeping.value) {
                    let item = request.intent.slots.Housekeeping.resolutions.resolutionsPerAuthority[0].values[0].value.id;
                    //const response = await registerRequest(amount, item);
                    let reqid_param = 'SR';
                    let req_id_str = await timestamp(request.timestamp);
                    let requestDepartment = 'Housekeeping'
                    let intentName = 'HousekeepingIntent';
                    let requestTypeValue = 'services'
                    let slotValues = {
                        item: item,
                        amount: null,
                    }
                    const message = await registerRequest(requestTypeValue, requestDepartment, slotValues, deviceId, intentName);
                    let speechText = await randomArray(message.speechText);
                    console.log(speechText);
                    speechText = speechText.replace('@Housekeeping', item);
                    console.log('finalmessage', speechText);
                    if (message) {
                        return handlerInput.responseBuilder
                            .speak(speechText)
                            .reprompt('what else?')
                            .getResponse();
                    } else {
                        return handlerInput.responseBuilder
                            .speak('Aplogies! there was a problem placing this order. ')
                            .reprompt('Anything else I can help you with? ')
                            .getResponse();
                    }
                }
            }
                break;
            case 'EngineeringIntent': {
                if (request.intent.slots.Complaints
                    && request.intent.slots.Complaints.resolutions
                    && request.intent.slots.Complaints.resolutions.resolutionsPerAuthority[0]
                    && request.intent.slots.Complaints.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH'
                    && request.intent.slots.Complaints.value !== "?" && request.intent.slots.Complaints.value) {
                    let item = request.intent.slots.Complaints.resolutions.resolutionsPerAuthority[0].values[0].value.id;
                    let reqid_param = 'EN';
                    let req_id_str = await timestamp(request.timestamp);
                    let intentName = 'EngineeringIntent';
                    let requestTypeValue = 'engineering';
                    let requestDepartment = 'Engineering'

                    let slotValues = {
                        item: item,
                        amount: null,
                    }

                    const message = await registerRequest(requestTypeValue, requestDepartment, slotValues, deviceId, intentName);
                    let speechText = await randomArray(message.speechText);
                    console.log(speechText);
                    speechText = speechText.replace('@Complaints', item);
                    console.log('finalmessage', speechText);

                    if (message) {
                        return handlerInput.responseBuilder
                            .speak(speechText)
                            .reprompt('what else?')
                            .getResponse();
                    } else {
                        return handlerInput.responseBuilder
                            .speak('order not placed')
                            .reprompt('Anything else I can help you with? ')
                            .getResponse();
                    }
                }
            }
                break;
            case 'LaundryIntent': {
                let intentName = 'LaundryIntent';
                let requestTypeValue = 'laundry';
                let requestDepartment = 'Housekeeping'

                let slotValues = {
                    item: "laundry",
                    amount: null,
                }

                const message = await registerRequest(requestTypeValue, requestDepartment, slotValues, deviceId, intentName);
                console.log('RESPONSE====> ', message.speechText);
                let speechText = await randomArray(message.speechText);
                console.log(speechText);
                speechText = speechText.replace('@laundry', item);
                // speechText = speechText.replace('@number', amount);
                console.log('finalmessage', speechText);

                if (message) {
                    return handlerInput.responseBuilder
                        .speak('order placed')
                        .reprompt('what else?')
                        .getResponse();
                } else {
                    return handlerInput.responseBuilder
                        .speak('order not placed')
                        .reprompt('Anything else I can help you with? ')
                        .getResponse();
                }
            }
                break;

            default: return handlerInput.responseBuilder
                .speak(`Sorry, I couldn't get you. Please say again? `)
                .reprompt(`Sorry, I couldn't get you. Please say again? `)
                .getResponse();
        }

        return handlerInput.responseBuilder
            .speak(`Sorry, I couldn't get you. Please say again? `)
            .reprompt(`Sorry, I couldn't get you. Please say again? `)
            .getResponse();
    }
};

const BellBoyHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'BellBoyIntent'
    }, async handle(handlerInput) {
        let request = handlerInput.requestEnvelope.request;
        let deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
        let requestDepartment = 'FrontDesk'
        let intentName = 'BellBoyIntent';
        let requestTypeValue = 'bellboy'
        let slotValues = {
            item: 'bell boy',
            amount: null,
        }
        const message = await registerRequest(requestTypeValue, requestDepartment, slotValues, deviceId, intentName);
        console.log('RESPONSE====> ', message.speechText);
        let speechText = await randomArray(message.speechText);
        console.log(speechText);
        // speechText = speechText.replace('@laundry', item);
        // speechText = speechText.replace('@number', amount);
        console.log('finalmessage', speechText);

        if (message) {
            return handlerInput.responseBuilder
                .speak(speechText)
                .reprompt('what else?')
                .getResponse();
        } else {
            return handlerInput.responseBuilder
                .speak('order not placed')
                .reprompt('Anything else I can help you with? ')
                .getResponse();
        }
    }
};

const BookSpaHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'BookSpaIntent'
            && request.dialogState === 'COMPLETED'
    }, async handle(handlerInput) {
        let request = handlerInput.requestEnvelope.request;
        let deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
        console.log('deviceId is', deviceId);
        let reqid_param = 'BS';
        let req_id_str = await timestamp(request.timestamp);
        let intentName = 'BookSpaIntent';
        let spaTime = request.intent.slots.Time.value;
        let spaDate = request.intent.slots.Date.value;
        let spaNumber = request.intent.slots.Number.value;

        let dateSplit = spaDate.split("-");
        let spaDay = dateSplit[2];
        let spaMonth = dateSplit[1];
        let spaYear = dateSplit[0];

        let spaBookTime = spaTime.toString();
        let replaceSpaTime = spaBookTime.replace(":", ".");
        console.log("replace Spa Time" + replaceSpaTime);
        let convertSpa = parseFloat(replaceSpaTime);
        console.log("Convert Spa" + convertSpa);

        var myDate = new Date();
        var hours = myDate.getUTCHours() + 5;
        var minutes = myDate.getUTCMinutes() + 30;
        var currentDay = myDate.getDate();
        var currentMonth = myDate.getMonth() + 1;
        var currentYear = myDate.getFullYear();

        if (minutes >= 60) {
            hours = hours + 1;
            minutes = minutes - 60;
        }
        else if (hours >= 24) {
            hours = hours - 24;
        }
        else {
            hours = hours;
            minutes = minutes;
        }

        var hour1 = hours.toString();
        var minute1 = minutes.toString();

        var hoursMeal = hour1.concat(".");
        var minutesMeal = hoursMeal.concat(minute1);
        var timeMeal = parseFloat(minutesMeal); //current time


        if ((spaMonth == currentMonth) && (spaDay < currentDay)) {

            return handlerInput.responseBuilder
                .speak(`Sorry your requested date has expired.`)
                .withShouldEndSession(true)
                .getResponse();
        }
        else if (spaMonth < currentMonth) {

            return handlerInput.responseBuilder
                .speak(`Sorry your requested date has expired.`)
                .withShouldEndSession(true)
                .getResponse();
        }
        else if ((spaMonth == currentMonth) && (spaDay == currentDay) && (convertSpa < timeMeal)) {
            return handlerInput.responseBuilder
                .speak(`Sorry your requested time has expired.`)
                .withShouldEndSession(true)
                .getResponse();
        }

        else {
            if (convertSpa >= (8.00) && convertSpa <= (20.30)) {
                let slotsSpa = [{
                    item: spaDate + " at " + spaTime,
                    amount: spaNumber
                }];
                console.log("i am inside bookspa intent");
                const message = await registerRequest(reqid_param, req_id_str, requestTypeValue, slotValues, deviceId, intentName);
                const message = await registerRequest(reqid_param, req_id_str, requestTypeValue, slotValues, deviceId, intentName);
                console.log('RESPONSE====> ', message.speechText);
                let speechText = await randomArray(message.speechText);
                speechText = speechText.replace('@time', spaTime);
                speechText = speechText.replace('@date', spaDate);
                speechText = speechText.replace('@number', spaNumber);
                console.log(speechText);
                if (message) {
                    return handlerInput.responseBuilder
                        .speak(speechText)
                        .reprompt('what else?')
                        .getResponse();
                } else {
                    return handlerInput.responseBuilder
                        .speak('booking not placed')
                        .reprompt('Anything else I can help you with? ')
                        .getResponse();
                }
            }
            else {
                this.response.speak("Sorry the spa is not open at your requested time. The spa timings are 8 a.m. to 8 30 p.m, please try booking the spa for this time ")
                    .shouldEndSession(true);
                this.emit(':responseReady');
            }
        }
    }
};

const BookCabHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'BookCabIntent'
            && request.dialogState === 'COMPLETED'
    }, async handle(handlerInput) {
        let request = handlerInput.requestEnvelope.request;
        let deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
        console.log('deviceId is', deviceId);
        let reqid_param = 'BS';
        let req_id_str = await timestamp(request.timestamp);
        let intentName = 'BookCabIntent';
        let cabTime = request.intent.slots.Time.value;
        let cabDate = request.intent.slots.Date.value;
        let cabDestination = request.intent.slots.Destination.value;

        let dateSplit = cabDate.split("-");
        let cabDay = dateSplit[2];
        let cabMonth = dateSplit[1];
        let cabYear = dateSplit[0];

        let cabBookTime = cabTime.toString();
        let replacecabTime = cabBookTime.replace(":", ".");
        console.log("replace cab Time" + replacecabTime);
        let convertcab = parseFloat(replacecabTime);
        console.log("Convert cab" + convertcab);

        var myDate = new Date();
        var hours = myDate.getUTCHours() + 5;
        var minutes = myDate.getUTCMinutes() + 30;
        var currentDay = myDate.getDate();
        var currentMonth = myDate.getMonth() + 1;
        var currentYear = myDate.getFullYear();

        if (minutes >= 60) {
            hours = hours + 1;
            minutes = minutes - 60;
        }
        else if (hours >= 24) {
            hours = hours - 24;
        }
        else {
            hours = hours;
            minutes = minutes;
        }

        var hour1 = hours.toString();
        var minute1 = minutes.toString();

        var hoursMeal = hour1.concat(".");
        var minutesMeal = hoursMeal.concat(minute1);
        var timeMeal = parseFloat(minutesMeal); //current time


        if ((cabMonth == currentMonth) && (cabDay < currentDay)) {

            return handlerInput.responseBuilder
                .speak(`Sorry your requested date has expired.`)
                .withShouldEndSession(true)
                .getResponse();
        }
        else if (cabMonth < currentMonth) {

            return handlerInput.responseBuilder
                .speak(`Sorry your requested date has expired.`)
                .withShouldEndSession(true)
                .getResponse();
        }
        else if ((cabMonth == currentMonth) && (cabDay == currentDay) && (convertcab < timeMeal)) {
            return handlerInput.responseBuilder
                .speak(`Sorry your requested time has expired.`)
                .withShouldEndSession(true)
                .getResponse();
        }

        else {
            let slotscab = [{
                item: cabDate + " at " + cabTime,
                amount: cabNumber
            }];
            console.log("i am inside bookcab intent");
            const message = await registerRequest(reqid_param, req_id_str, requestTypeValue, requestDepartment, slotValue, deviceId, intentName);
            console.log('RESPONSE====> ', message.speechText);

            let speechText = await randomArray(message.speechText);
            speechText = speechText.replace('@time', cabTime);
            speechText = speechText.replace('@date', cabDate);

            if (cabDestination) {
                speechText = speechText.replace('@destination', ' ');
            }

            console.log(speechText);
            if (message) {
                return handlerInput.responseBuilder
                    .speak(speechText)
                    .reprompt('what else?')
                    .getResponse();
            } else {
                return handlerInput.responseBuilder
                    .speak('booking not placed')
                    .reprompt('Anything else I can help you with? ')
                    .getResponse();
            }
        }
    }
};

const ExtendedCheckOutHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'ExtendedCheckOutIntent'
            && request.dialogState === 'COMPLETED'
    }, async handle(handlerInput) {
        let request = handlerInput.requestEnvelope.request;
        let deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
        let reqid_param = 'ECO';
        let req_id_str = await timestamp(request.timestamp);
        let requestDepartment = 'FrontDesk'
        let intentName = 'ExtendedCheckOutIntent';
        let requestTypeValue = 'ExtendedCheckOut'
        let numberOfHours = request.intent.slots.Number.value;
        let time = request.intent.slots.Time.value;
        let slotValues = {};
        if (numberOfHours) {
            slotValues = {
                item: `for ${numberOfHours} hours`,
                amount: 0,
            }
        } else if (time) {
            slotValues = {
                item: `till ${time} `,
                amount: 0,
            }
        }
        const message = await registerRequest(requestTypeValue, requestDepartment, slotValues, deviceId, intentName);
        console.log('RESPONSE====> ', message.speechText);
        let speechText = await randomArray(message.speechText);
        console.log(speechText);
        speechText = speechText.replace('@time', time);
        speechText = speechText.replace('@number', numberOfHours);
        console.log('finalmessage', speechText);

        if (message) {
            return handlerInput.responseBuilder
                .speak(speechText)
                .reprompt('what else?')
                .getResponse();
        } else {
            return handlerInput.responseBuilder
                .speak('order not placed')
                .reprompt('Anything else I can help you with? ')
                .getResponse();
        }
    }
};

const CheckoutHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'CheckoutIntent'
    }, async handle(handlerInput) {
        const responseBuilder = handlerInput.responseBuilder;
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        attributes.Checkout = true;
        handlerInput.attributesManager.setSessionAttributes(attributes); //session attributes

        if (supportsDisplay(handlerInput)) {
            // const image = new Alexa.ImageHelper()
            //   .addImageInstance(getLargeImage(selectedState.Abbreviation))
            //   .getImage();
            const bgImage = new alexa.ImageHelper()
                .addImageInstance("https://s3.amazonaws.com/hospitalityalexa/Checkout/checkout2.png")
                .getImage();
            let speechOutput = `Would you like to proceed with your checkout request?`;
            const primaryText = new alexa.RichTextContentHelper()
                .withPrimaryText(speechOutput)
                .getTextContent();
            responseBuilder.addRenderTemplateDirective({
                type: "BodyTemplate6",
                backButton: 'HIDDEN',
                backgroundImage: bgImage,
                title: "Checkout",
                textContent: primaryText,
            });
            return responseBuilder.speak(speechOutput)
                .reprompt(speechOutput)
                .getResponse();
        } else {
            let speechOutput = `Would you like to proceed with your checkout request?`;

            return responseBuilder.speak(speechOutput)
                .reprompt(speechOutput)
                .getResponse();
        }
    }
};

const FeedbackHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'FeedbackIntent'
    }, handle(handlerInput) {
        //use slots for happy neutral sad in one intent Feedback
        const request = handlerInput.requestEnvelope.request;
        const responseBuilder = handlerInput.responseBuilder;
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        let speechText = ``;
        let title = '';

        //GET API call to get the feedback questions 

        if (attributes.Question_1 === true) {
            attributes.Question_1 = false;
            attributes.Question_2 = true;
            attributes.Feedback = [];

            speechText += `How do you rate our Ambience? You can rate us between one to five `

            if (supportsDisplay(handlerInput)) {
                const happy = new alexa.ImageHelper()
                    .addImageInstance("https://s3.amazonaws.com/hospitalityalexa/Feedback/happy.png")
                    .getImage();

                const neutral = new alexa.ImageHelper()
                    .addImageInstance("https://s3.amazonaws.com/hospitalityalexa/Feedback/neutral.png")
                    .getImage();

                const sad = new alexa.ImageHelper()
                    .addImageInstance("https://s3.amazonaws.com/hospitalityalexa/Feedback/sad.png")
                    .getImage();

                let listItem = [];
                listItem.push({
                    token: 'happy',
                    textContent: new alexa.PlainTextContentHelper()
                        .withPrimaryText("Happy")
                        .getTextContent(),
                    image: happy,
                });

                listItem.push({
                    token: 'neutral',
                    textContent: new alexa.PlainTextContentHelper()
                        .withPrimaryText("Neutral")
                        .getTextContent(),
                    image: neutral,
                });

                listItem.push({
                    token: 'sad',
                    textContent: new alexa.PlainTextContentHelper()
                        .withPrimaryText("Sad")
                        .getTextContent(),
                    image: sad,
                });

                responseBuilder.addRenderTemplateDirective({
                    type: `ListTemplate2`,
                    token: 'listToken',
                    backButton: 'hidden',
                    title: `Housekeeping Services`,
                    listItems: listItem,
                });
            }
        } else {
            let feedback = request.intent.slots.feedback.value;
            if (attributes.Question_2 === true) {
                attributes.Question_2 = false;
                attributes.Question_3 = true;
                attributes.Feedback.push({ Question: "Ambience", Answer: feedback });
                speechText += `How do you rate our Staff Services? `;
                title += '';

            } else if (attributes.Question_3 === true) {
                attributes.Question_3 = false;
                attributes.Question_4 = true;
                attributes.Feedback.push({ Question: "Staff Service", Answer: feedback });
                speechText += `How do you rate our Food and Beverage? `;
                title += '';

            } else if (attributes.Question_4 === true) {
                attributes.Question_4 = false;
                attributes.Feedback.push({ Question: "Food and Beverage", Answer: feedback });
                speechText += `How do you rate our Elaachi Service? `;
                title += '';
            } else {
                attributes.Feedback.push({ Question: "Elaachi Service", Answer: feedback });
                return ThankyouHandler.handle(handlerInput);
            }
        }
        if (supportsDisplay(handlerInput)) {
            const happy = new alexa.ImageHelper()
                .addImageInstance("https://s3.amazonaws.com/hospitalityalexa/Feedback/happy.png")
                .getImage();

            const neutral = new alexa.ImageHelper()
                .addImageInstance("https://s3.amazonaws.com/hospitalityalexa/Feedback/neutral.png")
                .getImage();

            const sad = new alexa.ImageHelper()
                .addImageInstance("https://s3.amazonaws.com/hospitalityalexa/Feedback/sad.png")
                .getImage();

            let listItem = [];
            listItem.push({
                token: 'happy',
                textContent: new alexa.PlainTextContentHelper()
                    .withPrimaryText("Happy")
                    .getTextContent(),
                image: happy,
            });

            listItem.push({
                token: 'neutral',
                textContent: new alexa.PlainTextContentHelper()
                    .withPrimaryText("Neutral")
                    .getTextContent(),
                image: neutral,
            });

            listItem.push({
                token: 'sad',
                textContent: new alexa.PlainTextContentHelper()
                    .withPrimaryText("Sad")
                    .getTextContent(),
                image: sad,
            });

            responseBuilder.addRenderTemplateDirective({
                type: `ListTemplate2`,
                token: 'listToken',
                backButton: 'hidden',
                title: title,
                listItems: listItem,
            });
        }

        handlerInput.attributesManager.setSessionAttributes(attributes); //session attributes
        return responseBuilder.speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};

const ThankyouHandler = {
    async handle(handlerInput) {
        //post API call to register the Feedback
        let speechOutput = `Thank you for your feedback! `;
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        let feedback = attributes.Feedback;
        const responseBuilder = handlerInput.responseBuilder;
        await registerFeedback(feedback);
        if (supportsDisplay(handlerInput)) {
            const bgImage = new alexa.ImageHelper()
                .addImageInstance("https://s3.amazonaws.com/hospitalityalexa/Checkout/checkout2.png")
                .getImage();
            const primaryText = new alexa.RichTextContentHelper()
                .withPrimaryText(speechOutput)
                .getTextContent();
            responseBuilder.addRenderTemplateDirective({
                type: "BodyTemplate6",
                backButton: 'HIDDEN',
                backgroundImage: bgImage,
                title: "Thank you",
                textContent: primaryText,
            });
        }
        return responseBuilder.speak(speechOutput)
            .withShouldEndSession(true)
            .getResponse();
    }
};

function supportsDisplay(handlerInput) {
    const hasDisplay =
        handlerInput.requestEnvelope.context &&
        handlerInput.requestEnvelope.context.System &&
        handlerInput.requestEnvelope.context.System.device &&
        handlerInput.requestEnvelope.context.System.device.supportedInterfaces &&
        handlerInput.requestEnvelope.context.System.device.supportedInterfaces.Display;
    return hasDisplay;
}

const InProgressFoodIntent = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'OrderFoodIntent'
            && request.dialogState !== 'COMPLETED';
    },
    async handle(handlerInput) {
        const currentIntent = handlerInput.requestEnvelope.request.intent;
        let prompt = '';
        const attributes = handlerInput.attributesManager.getSessionAttributes();

        for (const slotName in currentIntent.slots) {
            if (Object.prototype.hasOwnProperty.call(currentIntent.slots, slotName)) {
                const currentSlot = currentIntent.slots[slotName];
                if (currentSlot.confirmationStatus !== 'CONFIRMED'
                    && currentSlot.resolutions
                    && currentSlot.resolutions.resolutionsPerAuthority[0]) {
                    if (currentSlot.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH') {
                        let item = request.intent.slots.Food.value;
                        const result = await searchFoodItem(item);
                        console.log("After searching", result);
                        if (result) {
                            attributes.foodItemDetails = result;
                            handlerInput.attributesManager.setSessionAttributes(attributes);
                            if (result.length > 1) {
                                // give options
                                speechText += `Would you like to have `
                                for (let i = 0; i < result.length; i++) {
                                    speechText += `${result[i].foodItem} <break time="0.7s"/> `;
                                }
                                return handlerInput.responseBuilder
                                    .speak(speechText)
                                    .reprompt(speechText)
                                    .addElicitSlotDirective(currentSlot.name)
                                    .getResponse();
                            }
                        } else {
                            speechText += `Sorry I couldn't find ${item} in the restaurant's menu. Please try ordering something from the menu. `
                        }
                        // if (currentSlot.resolutions.resolutionsPerAuthority[0].values.length > 1) {

                        //     prompt = 'Which would you like';
                        //     const size = currentSlot.resolutions.resolutionsPerAuthority[0].values.length;

                        //     currentSlot.resolutions.resolutionsPerAuthority[0].values
                        //         .forEach((element, index) => {
                        //             prompt += ` ${(index === size - 1) ? ' or' : ' '} ${element.value.name}`;
                        //         });

                        //     prompt += '?';

                        //     return handlerInput.responseBuilder
                        //         .speak(prompt)
                        //         .reprompt(prompt)
                        //         .addElicitSlotDirective(currentSlot.name)
                        //         .getResponse();
                        // }

                    } else if (currentSlot.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_NO_MATCH') {
                        if (requiredSlots.indexOf(currentSlot.name) > -1) {
                            prompt = `What ${currentSlot.name} are you looking for`;

                            return handlerInput.responseBuilder
                                .speak(prompt)
                                .reprompt(prompt)
                                .addElicitSlotDirective(currentSlot.name)
                                .getResponse();
                        }
                    }
                }
            }
        }

        return handlerInput.responseBuilder
            .addDelegateDirective(currentIntent)
            .getResponse();
    },
};

const OrderFoodIntent = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'OrderFoodIntent'
            && request.dialogState === 'COMPLETED';
    }, async handle(handlerInput) {
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        let request = handlerInput.requestEnvelope.request;
        let speechText = "";
        let foodList = [];
        if (request.intent.slots.Food
            && request.intent.slots.Food.resolutions
            && request.intent.slots.Food.resolutions.resolutionsPerAuthority[0]
            && request.intent.slots.Food.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH'
            && request.intent.slots.Food.value !== "?" && request.intent.slots.Food.value) {

            let item = request.intent.slots.Food.resolutions.resolutionsPerAuthority[0].values[0].value.name;
            let amount = request.intent.slots.Quantity.value;
            const result = await searchFoodItem(item);
            console.log("resultFOood=>>>>>>>>", result.data[0])
            // let foodItemDetails = attributes.foodItemDetails;
            if (attributes.foodList) {
                foodList = attributes.foodList;
                foodList.push({ item: item, amount: amount });
                attributes.foodList = foodList;
            } else {
                foodList.push({ item: item, amount: amount });
                attributes.foodList = foodList;
            }
            speechText += `${amount} ${item} added to the cart. `

            if (result.data[0].recommendedWith) {
                attributes.recommendedItem = result.data[0].recommendedWith;
                attributes.recommendedWith = true;
                speechText += `Would you like to try ${result.data[0].recommendedWith} with ${item} as well?`
            } else {
                speechText += `Would you like to have any thing else? `
            }

            handlerInput.attributesManager.setSessionAttributes(attributes);

            return handlerInput.responseBuilder
                .speak(speechText)
                .reprompt(speechText)
                .getResponse();
        } else {
            return handlerInput.responseBuilder
                .speak(`Sorry, I couldn't get you. Please say again? `)
                .reprompt(`Sorry, I couldn't get you. Please say again? `)
                .getResponse();
        }
    }
}

const RemoveFoodHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'RemoveFoodItem';
    }, async handle(handlerInput) {
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        let request = handlerInput.requestEnvelope.request;
        let foodList = attributes.foodList;
        const responseBuilder = handlerInput.responseBuilder;
        let speechText = " ";

        if (request.intent.slots.Food
            && request.intent.slots.Food.resolutions
            && request.intent.slots.Food.resolutions.resolutionsPerAuthority[0]
            && request.intent.slots.Food.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH'
            && request.intent.slots.Food.value !== "?" && request.intent.slots.Food.value) {

            let slotsRemoveFood = request.intent.slots.Food.resolutions.resolutionsPerAuthority[0].values[0].value.name;
            let amount = request.intent.slots.Quantity.value;
            let listItem = [];
            if (amount) {
                for (let i = 0; i < foodList.length; i++) {
                    //var similarity = stringSimilarity.compareTwoStrings(slotsRemoveFood, foodList[i].Item);
                    var similarity = foodList[i].item.startsWith(slotsRemoveFood);
                    console.log(similarity);
                    console.log(slotsRemoveFood);
                    console.log(foodList[i].item);
                    if (similarity == true) {
                        let foodItem = foodList[i];
                        let foodQuantity = (foodItem.amount - amount);
                        foodList[i].amount = foodQuantity;
                        if (foodQuantity == 0) {
                            foodList.splice(i, 1);
                        }
                        attributes.foodList = foodList;
                        imageURL = `https://s3.amazonaws.com/smartgenie/Echo+Show+Cards/roomservice.png`

                        if (attributes.foodList.length != 0) {

                            attributes.foodList.forEach((element) => {
                                const pizza = new alexa.ImageHelper()
                                    .addImageInstance("https://s3.amazonaws.com/smartgenie/Echo+Show+Cards/icons.png")
                                    .getImage();

                                listItem.push({
                                    token: '',
                                    textContent: new alexa.PlainTextContentHelper()
                                        .withPrimaryText(`<font size='5'>${element.amount} x ${element.item}</font>`)
                                        .getTextContent(),
                                    image: pizza,
                                });
                                console.log(`${element.amount} ${element.item}`)
                            });

                            speechText += " item removed. Please say done so that i may repeat your order";
                            if (supportsDisplay(handlerInput)) {

                                const bgImage = new alexa.ImageHelper()
                                    .addImageInstance(imageURL)
                                    .getImage();

                                responseBuilder.addRenderTemplateDirective({
                                    type: "ListTemplate1",
                                    backButton: 'HIDDEN',
                                    backgroundImage: bgImage,
                                    title: title,
                                    listItems: listItem,
                                });
                            }
                        }
                        else {
                            speechText = + `Your cart is empty!`
                            if (supportsDisplay(handlerInput)) {
                                const bgImage = new alexa.ImageHelper()
                                    .addImageInstance(imageURL)
                                    .getImage();
                                const primaryText = new alexa.RichTextContentHelper()
                                    .withPrimaryText(`<font size="7"> ${speechText} </font>`)
                                    .getTextContent();
                                responseBuilder.addRenderTemplateDirective({
                                    type: "BodyTemplate6",
                                    backButton: 'HIDDEN',
                                    backgroundImage: bgImage,
                                    title: title,
                                    textContent: primaryText,
                                });
                            }
                        }

                    }
                }
            } else {
                for (let i = 0; i < foodList.length; i++) {
                    //var similarity = stringSimilarity.compareTwoStrings(slotsRemoveFood, foodList[i].Item);
                    var similarity = foodList[i].item.startsWith(slotsRemoveFood);
                    console.log(similarity);
                    console.log(slotsRemoveFood);
                    console.log(foodList[i].item);
                    if (similarity == true) {

                        foodList.splice(i, 1);
                        attributes.foodList = foodList;
                        imageURL = `https://s3.amazonaws.com/smartgenie/Echo+Show+Cards/roomservice.png`

                        if (attributes.foodList.length != 0) {

                            attributes.foodList.forEach((element) => {
                                const pizza = new alexa.ImageHelper()
                                    .addImageInstance("https://s3.amazonaws.com/smartgenie/Echo+Show+Cards/icons.png")
                                    .getImage();

                                listItem.push({
                                    token: '',
                                    textContent: new alexa.PlainTextContentHelper()
                                        .withPrimaryText(`<font size='5'>${element.amount} x ${element.item}</font>`)
                                        .getTextContent(),
                                    image: pizza,
                                });
                                console.log(`${element.amount} ${element.item}`)
                            });

                            speechText += " item removed. Please say done so that i may repeat your order";
                            if (supportsDisplay(handlerInput)) {

                                const bgImage = new alexa.ImageHelper()
                                    .addImageInstance(imageURL)
                                    .getImage();

                                responseBuilder.addRenderTemplateDirective({
                                    type: "ListTemplate1",
                                    backButton: 'HIDDEN',
                                    backgroundImage: bgImage,
                                    title: title,
                                    listItems: listItem,
                                });
                            }
                        }
                        else {
                            speechText = + `Your cart is empty!`
                            if (supportsDisplay(handlerInput)) {
                                const bgImage = new alexa.ImageHelper()
                                    .addImageInstance(imageURL)
                                    .getImage();
                                const primaryText = new alexa.RichTextContentHelper()
                                    .withPrimaryText(`<font size="7"> ${speechText} </font>`)
                                    .getTextContent();
                                responseBuilder.addRenderTemplateDirective({
                                    type: "BodyTemplate6",
                                    backButton: 'HIDDEN',
                                    backgroundImage: bgImage,
                                    title: title,
                                    textContent: primaryText,
                                });
                            }
                        }

                    }
                }
            }
            handlerInput.attributesManager.setSessionAttributes(attributes);
        } else {
            speechText += `Sorry, I didn't get you?`;
        }
        return responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
}

const YesHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'AMAZON.YesIntent'
    }, async handle(handlerInput) {
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        const responseBuilder = handlerInput.responseBuilder;
        let speechText = ``;
        let imageURL = ``
        let title = ``;
        if (attributes.recommendedWith === true) {
            attributes.recommendedWith = false;
            let recommendedItem = attributes.recommendedItem;
            let foodList = [];

            if (attributes.foodList) {
                foodList = attributes.foodList;
                foodList.push({ item: recommendedItem, amount: 1 });
                attributes.foodList = foodList;
            } else {
                foodList.push({ item: recommendedItem, amount: 1 });
                attributes.foodList = foodList;
            }
            speechText += `1 ${recommendedItem} added to the cart. say confirm to place the order. `

        } else if (attributes.Checkout === true) {
            attributes.Checkout = false;
            attributes.CheckoutFeedback = true;
            title = 'Checkout'
            imageURL = `https://s3.amazonaws.com/hospitalityalexa/Checkout/checkout.png`
            speechText = `Okay, I am preparing your check out, till then would you like to give feedback of your stay? `
            if (supportsDisplay(handlerInput)) {
                const bgImage = new alexa.ImageHelper()
                    .addImageInstance(imageURL)
                    .getImage();
                const primaryText = new alexa.RichTextContentHelper()
                    .withPrimaryText(speechText)
                    .getTextContent();
                responseBuilder.addRenderTemplateDirective({
                    type: "BodyTemplate6",
                    backButton: 'HIDDEN',
                    backgroundImage: bgImage,
                    title: title,
                    textContent: primaryText,
                });
            }
        } else if (attributes.CheckoutFeedback === true) {
            attributes.CheckoutFeedback = false;
            attributes.Question_1 = true;
            return FeedbackHandler.handle(handlerInput);
        }

        handlerInput.attributesManager.setSessionAttributes(attributes);

        return responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
}

const NoHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'AMAZON.NoIntent'
    }, async handle(handlerInput) {
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        const responseBuilder = handlerInput.responseBuilder;
        let speechText = ``;
        let imageURL = ``
        let title = ``;
        let shouldEndSession = false;
        if (attributes.recommendedWith === true) {
            attributes.recommendedWith = false;
            speechText += `Okay! Would you like to order anything else? say confirm to place the order. `
        } else {
            shouldEndSession = true;
            speechText += `Thanks for using Alexa Hotel Services`
        }
        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(shouldEndSession)
            .getResponse();
    }
}

const ConfirmFoodOrderIntent = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'ConfirmFoodIntent';
    }, async handle(handlerInput) {
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        let deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
        console.log('deviceId is', deviceId);
        let request = handlerInput.requestEnvelope.request;
        let reqid_param = 'FO';
        let req_id_str = await timestamp(request.timestamp);
        let intentName = 'FoodIntent';
        let requestTypeValue = 'Food'
        let slotValues = attributes.foodList;
        const message = await registerRequest(reqid_param, req_id_str, requestTypeValue, slotValues, deviceId, intentName);
        console.log('RESPONSE====> ', message.speechText);
        let speechText = await randomArray(message.speechText);
        console.log(speechText);
        attributes.foodList = [];
        handlerInput.attributesManager.setSessionAttributes(attributes);
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt("what else?")
            .getResponse();
    }
}

const ShowMenuIntent = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'ShowMenuIntent';
    }, async handle(handlerInput) {
        const foodMenu = await getFoodMenu();
        console.log("FOOD MENU=======>>>>>", foodMenu)
        let speechText = "";
        let request = handlerInput.requestEnvelope.request;
        if (request.intent.slots.foodCategory
            && request.intent.slots.foodCategory.resolutions
            && request.intent.slots.foodCategory.resolutions.resolutionsPerAuthority[0]
            && request.intent.slots.foodCategory.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH'
            && request.intent.slots.foodCategory.value !== "?" && request.intent.slots.foodCategory.value) {

            let foodCategory = request.intent.slots.foodCategory.resolutions.resolutionsPerAuthority[0].values[0].value.name;
            // let foodCategoryLower = foodCategory.toLowerCase();
            // const foodItemDetails = await searchData(foodMenu, 'cheese chicken');
            // console.log("foodItemDetails", foodItemDetails);
            console.log("FOOD category", foodCategory);
            foodCategory = foodCategory.toLowerCase();
            if (foodMenu.hasOwnProperty(foodCategory)) {
                let arrayOfFoodItems = foodMenu[foodCategory];
                // console.log("arrayOfFoodCategory", arrayOfFoodCategory)

                speechText += `In ${foodCategory} we have `
                for (let i = 0; i < arrayOfFoodItems.length; i++) {
                    speechText += `${arrayOfFoodItems[i].foodItem}, `
                }
            } else {
                speechText += `Sorry we don't have a food category name as ${foodCategory}`
            }
            //image kaha se aayegi?
            speechText += `What would you like to order? `

        } else {
            const foodItemDetails = await searchData(foodMenu, 'cheese burger');
            console.log("foodItemDetails", foodItemDetails);

            // const foodItemDetails1 = await searchData(foodMenu, 'pizza magic');
            // console.log("foodItemDetails11111", foodItemDetails1);

            let arrayOfFoodCategory = Object.keys(foodMenu);
            console.log("arrayOfFoodCategory", arrayOfFoodCategory)
            speechText += `In Menu we have `
            for (let i = 0; i < arrayOfFoodCategory.length; i++) {
                speechText += `${arrayOfFoodCategory[i]}, `
            }
            //image kaha se aayegi?
            speechText += `What would you like to order? `
        }
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
}

const WakeUpCallHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        console.log('WAKEWAKE', request);
        return request.type === 'IntentRequest'
            && request.intent.name === 'WakeupCallIntent'
            && request.dialogState === 'COMPLETED';
    }, async handle(handlerInput) {
        let deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
        console.log('deviceId is', deviceId);
        let request = handlerInput.requestEnvelope.request;
        let reqid_param = 'WC';
        let req_id_str = await timestamp(request.timestamp);
        let intentName = 'WakeupCallIntent';
        let requestDepartment = 'FrontDesk'
        let requestTypeValue = 'WakeupCall'
        //time and cup of coffee?
        let time = request.intent.slots.Time.value;
        let date = request.intent.slots.Date.value;
        let teaCoffee = request.intent.slots.TeaCoffee.value;
        let slotValues = {};

        if (teaCoffee === 'yes') {
            if (date) {
                slotValues.item = `at ${time.toString()} on ${date.toString()} with coffee`;
                slotValues.amount = 0
            } else {
                slotValues.item = `at ${time.toString()} with coffee`;
                slotValues.amount = 0
            }
        } else {
            if (date) {
                slotValues.item = `at ${time.toString()} on ${date.toString()}`;
                slotValues.amount = 0
            } else {
                slotValues.item = `at ${time.toString()}`;
                slotValues.amount = 0
            }
        }
        var speechText = '';
        const message = await registerRequest(reqid_param, req_id_str, requestTypeValue, requestDepartment, slotValue, deviceId, intentName);
        console.log("message", message);
        if (message) {
            speechText = await randomArray(message.speechText);
            console.log(speechText);
            speechText = speechText.replace('@time', time);
            // speechText = speechText.replace('@number', amount);
            console.log('finalmessage', speechText);
            console.log(message);
        } else {
            speechText += 'Sorry request not placed'
        }
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt("Anything else I can help you with?")
            .getResponse();
    }
}

const WifiHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'WifiIntent';
    }, async handle(handlerInput) {
        let intentName = `WifiIntent`
        const responseData = await getResponse(intentName);
        console.log(responseData);
        return handlerInput.responseBuilder
            .speak("For connect to the hotel's Wi fi, go to the wi fi connections on your phone, select Elaachi wi fi and sign up with your room number and your last name. ")
            .reprompt("Anything else I can help you with?")
            .getResponse();
    }
}

const CheckStatusHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'CheckStatusIntent';
    }, async handle(handlerInput) {
        let intentName = `CheckStatusIntent`
        // const responseData = await getResponse(intentName);
        // console.log(responseData);
        let speechText = ``;
        let deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
        let request = handlerInput.requestEnvelope.request;

        if (request.intent.slots.requeststatus
            && request.intent.slots.requeststatus.resolutions
            && request.intent.slots.requeststatus.resolutions.resolutionsPerAuthority[0]
            && request.intent.slots.requeststatus.resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH'
            && request.intent.slots.requeststatus.value !== "?" && request.intent.slots.requeststatus.value) {
            let requestType = request.intent.slots.requeststatus.resolutions.resolutionsPerAuthority[0].values[0].value.name;
            let roomInfo = await getRoomInfo(deviceId);
            let roomNum = roomInfo.data.response._id;
            let requestStatus = await checkReqStatus(roomNum, requestType);
            console.log('req', requestStatus);
            for (let i = 0; i < 3; i++) {
                speechText += `Your ${requestStatus[i].requestType} for ${requestStatus[i].slotValues[0].item} is ${requestStatus[i].requestStatus} <break time="0.9s"/>`
            }
        } else {
            speechText += `Sorry, i didn't get you! `
        }
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt("Anything else I can help you with?")
            .getResponse();
    }
}

// faq

const GenericCheckStatusHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'GenericCheckStatusIntent';
    }, async handle(handlerInput) {
        let intentName = `CheckStatusIntent`
        // const responseData = await getResponse(intentName);
        // console.log(responseData);
        let speechText = ``;
        let deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;
        let request = handlerInput.requestEnvelope.request;

        let roomInfo = await getRoomInfo(deviceId);
        let roomNum = roomInfo.data.response._id;
        let requestStatus = await genericRequestStatus(roomNum);
        console.log("REQ", requestStatus)
        for (let i = 0; i < 3; i++) {
            speechText += `Your ${requestStatus[i].requestType} for ${requestStatus[i].slotValues[0].item} is ${requestStatus[i].requestStatus} <break time="0.9s"/>`
        }
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt("Anything else I can help you with?")
            .getResponse();
    }
}

const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speechText = 'Thanks for using Alexa Hotel Services!';

        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(true)
            .getResponse();
    },
};

const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`Error handled: ${error.message}`);
        const message = 'Sorry, this is not a valid command. Please say help to hear what you can say.';

        return handlerInput.responseBuilder
            .speak(message)
            .reprompt(message)
            .getResponse();
    },
};

const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

        return handlerInput.responseBuilder.getResponse();
    },
};

const SystemExceptionHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'System.ExceptionEncountered';
    },
    handle(handlerInput) {
        console.log(`System exception encountered: ${handlerInput.requestEnvelope.request} ${handlerInput.requestEnvelope.request.reason}`);
    },
};

//greeting
//faq

const skillBuilder = alexa.SkillBuilders.standard();

exports.handler = skillBuilder
    .addRequestHandlers(
        LaunchRequestHandler,
        ProactiveEventHandler,
        InProgressAmenitiesIntent,
        AmenitiesIntent,
        InProgressFoodIntent,
        OrderFoodIntent,
        RemoveFoodHandler,
        ConfirmFoodOrderIntent,
        WakeUpCallHandler,
        WifiHandler,
        ShowMenuIntent,
        ExtendedCheckOutHandler,
        CheckoutHandler,
        BookSpaHandler,
        BookCabHandler,
        BellBoyHandler,
        GenericCheckStatusHandler,
        CheckStatusHandler,
        GeneralHandler,
        FeedbackHandler,
        CancelAndStopIntentHandler,
        SystemExceptionHandler,
        SessionEndedRequestHandler,
        YesHandler,
        NoHandler,
    )
    .addErrorHandlers(ErrorHandler)
    .lambda();
