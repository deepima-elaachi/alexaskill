async function createRequest(slotValues, hotelId, roomNum, reqid_param, req_id_str, deviceId){
    let Result = {
		result: {
			"roomNumber": roomNum,
			"hotelId": hotelId,
			"slotValues": slotValues,
			"reqid_param": reqid_param,
			"req_id_str": req_id_str,
			"deviceId": deviceId,
			"ETA": "Undefined",
			"status": "Pending",
			"requestId": createRequestId(reqid_param, roomNum, hotelId, req_id_str)
		}
    };
    return Result;
}

function createRequestId(reqid_param, room_number, hotel_id, req_id_str) {
	var req_1 = reqid_param.concat("/"); //L/
	var req_2 = req_1.concat(room_number); // L/304
	var req_3 = req_2.concat("/"); // L/304/
	var req_4 = req_3.concat(hotel_id); // L/304/HM
	var req_5 = req_4.concat("/"); // L/304/HM/
	var req_6 = req_5.concat(req_id_str); // L/304/HM/timestamp
	return req_6;
}

module.exports = {
    createRequest
}