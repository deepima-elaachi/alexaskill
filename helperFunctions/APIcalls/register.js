const {baseURL} = require('../config/creds');
const axios = require('axios');
const {getResponse} = require('./getResponse');
async function registerRequest(requestTypeValue, requestDepartment, slotValue, deviceId, intentName) {
    try {
        const roomDetails = await getRoomDetails(deviceId);
        console.log('HERE1111');

        let roomNum = roomDetails.data.response._id;
        console.log('HERE', roomNum);

        const requestId = new Date().valueOf();
        console.log('HERE', requestId);
        const results = await axios.post(`${baseURL}/orders/createRequest`, {
            // requestID: requestId,
            requestType: requestTypeValue,
            roomNumber: roomNum,
            requestDepartment: requestDepartment,
            slotValues: slotValue,
            requestStatus: "Pending",
            estimatedTime: "not assigned",
            requestTimeline: "Order Received"
        }).then(async function (response) {
            const responseData = await getResponse(intentName);

            console.log(response, responseData);
            return responseData.data;
        }).catch(function (error) {
            console.log(error);
        });
        console.log('RESULT', results)
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the register request api!')
    }
}

async function getRoomDetails(deviceId) {
    try {
        const results = axios.get(`${baseURL}/room/getRoomInfo?deviceId=2`).then(function (response) {
            console.log(response);
            return response;
        }).catch(function (error) {
            console.log(error);
        });
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the getRoomInfo api!')
    }
}

function createRequestId(reqid_param, room_number, req_id_str) {
    console.log('inside create Request')
    var req_1 = reqid_param.concat("/"); //L/
    console.log('inside create Request t', req_6);

    var req_2 = req_1.concat(room_number); // L/304
    console.log('inside create Request tw', req_2);

    var req_3 = req_2.concat("/"); // L/304/
    console.log('inside create Request twi', req_3);

    var req_6 = req_3.concat(req_id_str); // L/304/HM/timestamp
    console.log('inside create Request twice22', req_6);

    return req_6;
}

module.exports = {
    registerRequest
}