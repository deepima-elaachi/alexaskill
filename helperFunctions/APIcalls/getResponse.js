const axios = require('axios');
let { baseURL } = require('../config/creds');
async function getResponse(IntentName) {
    try {
        const results = axios.get(`${baseURL}/response/getResponseData?intentName=${IntentName}`).then(function (response) {
            console.log(response);
            return response;
        }).catch(function (error) {
            console.log(error);
        });
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the getResponse api!', err);
    }
}

module.exports = {
    getResponse
}