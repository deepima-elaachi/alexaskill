const axios = require('axios');
const { baseURL } = require('../config/creds');

async function getFoodMenu () {
    const results = await axios.get(`${baseURL}/food/getAllFoodMenu`, {
    }).then((response) => {
        console.log("FOOD MENY", response)
        return response.data;
    }).catch((err)=> {
        console.log("Error occured", err);
    })
    return results;
}

module.exports = {
    getFoodMenu
}