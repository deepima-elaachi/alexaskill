async function timestamp(time){
    var a = time; //2017-07-02T00:54:57Z
    var str = a.split('T');
    var date = str[0];
    var timeZ = str[1];
    var str2 = timeZ.split('Z');
    var time = str2[0];
    var res = date.concat("/");// 2017-07-02/
    var req_id_str = res.concat(time);// 2017-07-02/00:54:57
    return req_id_str;
}

module.exports = {
    timestamp
}