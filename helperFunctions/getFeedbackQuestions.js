const { baseURL } = require('./creds')
const axios = require('axios');

async function getFeedbackQuestions() {
    const results = axios.get(`${baseURL}/feedback/getQuestions`).then((response) => {
        return response;
    }).catch((err) => {
        console.log("Error occured while calling the get Feedback question api", err)
    });
    return results;
}

module.exports = { getFeedbackQuestions }