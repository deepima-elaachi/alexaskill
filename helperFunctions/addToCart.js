const { searchFoodItem } = require('./searchFoodItem');

async function addToCart(handlerInput, amount, item) {
    const attributes = handlerInput.attributesManager.getSessionAttributes();
    const result = await searchFoodItem(item);
    let foodList = [];
    //pass the name of the item to the API which will perform the search operation
    console.log("result", result);
    let speechText = ``;

    if (result) {
        if (result.length > 1) {
            // give options
            speechText += `Would you like to have `
            for (let i = 0; i < result.length; i++) {
                speechText += `${result[i].foodItem} <break time="0.7s"/> `;
            }
        } else {
            // add to cart

            if (attributes.foodList) {
                foodList = attributes.foodList;
                foodList.push({ item: item, amount: amount });
                attributes.foodList = foodList;
            } else {
                foodList.push({ item: item, amount: amount });
                attributes.foodList = foodList;
            }
            speechText += `${amount} ${item} added to the cart. `

        }
    } else {
        speechText += `Sorry I couldn't find ${item} in the restaurant's menu. Please try ordering something from the menu. `
    }

    handlerInput.attributesManager.setSessionAttributes(attributes); //session attributes
    return speechText;
}

module.exports = {
    addToCart
}