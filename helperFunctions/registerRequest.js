//api call which will register the request into the database
const axios = require('axios');
const { createRequest } = require('./createRequest');
const { saveToDatabase } = require('./saveToDatabase');
const { getResponse } = require('./getResponse');

let getRoomInfoURL = `https://gunw3l5nj5.execute-api.us-east-1.amazonaws.com/dev/getRoomInfo`

async function registerRequest(reqid_param, req_id_str, slotValues, deviceId, intentName) {
    //api makes the request and the response is sent back
    //fetch the room number and the hotelId based upon the deviceId
    //POST request where deviceId
    const roomDetails = await getRoomDetails(deviceId)
    console.log('roomDes', roomDetails.data.result.hotelId);
    //get the room number and hotel id and post the request to the database
    let hotelId = roomDetails.data.result.hotelId;
    let roomNum = roomDetails.data.result.roomNumber;
    const requestID = await createRequest(slotValues, hotelId, roomNum, reqid_param, req_id_str, deviceId);
    console.log('request===>>', requestID.result, requestID.result.hotelId);
    const saved = await saveToDatabase(requestID.result);
    console.log('SAVED', saved);
    //get the response from the API based upon the intent name
    let message = await getResponse(hotelId, intentName);
    console.log('message====>>>>>', message.data.Result.SpeakText); //it's an array
    let item = [Math.floor(Math.random() * message.data.Result.SpeakText.length)];
    let randomMessage = message.data.Result.SpeakText[item];
    console.log('randomMessage====.....>>', randomMessage);

    return randomMessage;
}

async function getRoomDetails(deviceId) {
    try {
        const results = axios.post(`${getRoomInfoURL}`, {
            deviceId: "2" //change deviceid
        }).then(function (response) {
            console.log(response);
            return response;
        }).catch(function (error) {
            console.log(error);
        });
        return results;
    } catch (err) {
        console.log('Error Occured while connecting with the getRoomInfo api!')
    }
}
module.exports = {
    registerRequest
}