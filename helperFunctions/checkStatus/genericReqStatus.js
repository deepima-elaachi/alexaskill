const { baseURL } = require('../config/creds');
const axios = require('axios');

async function genericReqStatus(roomNumber) {
    try {
        const results = axios.get(`${baseURL}/orders/genericCheckStatus`, {
            roomNumber: roomNumber,
        }).then(response => {
            console.log(response);
            return response.data;
        }).catch(err => {
            console.log(response);
        });
        return results;
    } catch (err) {
        console.log(err);
    }
}

module.exports = genericReqStatus;