const baseURL = 'http://ec2-3-84-86-255.compute-1.amazonaws.com:8080'
const client_id = 'amzn1.application-oa2-client.2a52e397bdef4d389e7b868caf6436be';
const client_secret = 'd4df5e7a2f4e40827124de2854abe7cf26e59ab1fddd5f04e5c6e38fa1387369';
const scope = 'alexa::proactive_events'
const grant_type = 'client_credentials';

module.exports = {
    baseURL,
    client_id,
    client_secret,
    scope,
    grant_type
}